with open('hosts_new', 'r') as dictFile:
    dict = dictFile.readlines()
    dict = [x[:-1] for x in dict]

with open('dns.log', 'r') as recordsFile:
    records = recordsFile.readlines()
    records = [x.split('\x09')[9] for x in records if not x.startswith('#')]
    
intersection = [x for x in records if x in dict]
print(len(intersection) / len(records) * 100)